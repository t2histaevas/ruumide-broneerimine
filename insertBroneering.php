<?php
include 'config.php';

// $aktiivneKasutajaKasutajanimi = "select kasutajanimi from aktiivnekasutaja order by ID desc limit 1";

// $aktiivneKasutajaID = "SELECT kasutaja_id FROM kasutaja WHERE kasutajanimi=(select kasutajanimi from aktiivnekasutaja order by ID desc limit 1)";

//URL, mis lehelt tuli..Sellega määrame ära, mis ruume broneerime
$URLString = $_SERVER['HTTP_REFERER'];

if (strpos($URLString, 'ruumA321') !== false) {
    $ruumi_id = 1;
    $ruumi_nimi = "A321";
}
else if (strpos($URLString, 'ruumA322') !== false) {
    $ruumi_id = 2;
    $ruumi_nimi = "A322";
}
else if (strpos($URLString, 'ruumA323') !== false) {
    $ruumi_id = 3;
    $ruumi_nimi = "A323";
}
else if (strpos($URLString, 'ruumA324') !== false) {
    $ruumi_id = 4;
    $ruumi_nimi = "A324";
}

$koikRead = "SELECT * FROM broneering WHERE ruumi_id = '$ruumi_id'";
$records = mysqli_query($conn, $koikRead);

$olemasAlgus = "SELECT bronni_algus FROM broneering WHERE ruumi_id = '$ruumi_id' ";
$olemasLopp = "SELECT bronni_lopp FROM broneering WHERE ruumi_id = '$ruumi_id' ";

$olemasAlgusRecords = mysqli_query($conn, $olemasAlgus);
$olemasLoppRecords = mysqli_query($conn, $olemasLopp);

//vaatame kas broneering sobib
$sobib = True;
for ($x = 0; $x <= mysqli_num_rows($records); $x++) {
    $algus1 = mysqli_fetch_assoc($olemasAlgusRecords);
    $lopp1 = mysqli_fetch_assoc($olemasLoppRecords);
    if (($lopp < $algus1['bronni_algus'] || $algus > $lopp1['bronni_lopp']) && ($lopp > $algus)) {
    } else {
        $sobib = False;
    }
}

$sql = "INSERT INTO broneering (ruumi_id, kasutaja_id, bronni_algus, bronni_lopp)
        VALUES ((SELECT ruumi_id FROM ruum WHERE ruumi_nimi = '$ruumi_nimi'), 
        (SELECT kasutaja_id FROM kasutaja WHERE kasutajanimi=(select kasutajanimi from aktiivnekasutaja order by ID desc limit 1)), 
        '$algus', 
        '$lopp')";


if($sobib){
    if ($conn->query($sql)==TRUE){
        header('Location: mainSuccess.php');
    } else {
        echo "Error: " . $sql . "<br>" . $conn -> error;
    }
} else {
    header('Location: mainWarning.php');
}
$conn -> close();