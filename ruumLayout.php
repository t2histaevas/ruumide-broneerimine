<?php
include 'config.php';

function implode_with_key($assoc, $inglue = '=', $outglue = '&')
{
    $return = null;
    foreach ($assoc as $tk => $tv) $return .= $outglue.$tk.$inglue.$tv;
    return substr($return,1);
}
$a = implode_with_key($assoc, $inglue = '=', $outglue = '&');
echo $a;
exit();

$sql = "SELECT * FROM ruum WHERE ruumi_nimi = (select aktiivse_ruumi_nimi from aktiivneruum order by ID desc limit 1)";

$records = mysqli_query($conn, $sql);

$sqlAlgus = "SELECT bronni_algus FROM broneering WHERE ruumi_nimi = (select aktiivse_ruumi_nimi from aktiivneruum order by ID desc limit 1) AND 
            kasutaja_id = (SELECT kasutaja_id FROM kasutaja WHERE kasutajanimi=(select kasutajanimi from aktiivnekasutaja order by ID desc limit 1))";

$sqlLopp = "SELECT bronni_lopp FROM broneering WHERE ruumi_nimi = (select aktiivse_ruumi_nimi from aktiivneruum order by ID desc limit 1) AND
            kasutaja_id = (SELECT kasutaja_id FROM kasutaja WHERE kasutajanimi=(select kasutajanimi from aktiivnekasutaja order by ID desc limit 1))";

$broneeringAlgusRecords = mysqli_query($conn, $sqlAlgus);

$broneeringLoppRecords = mysqli_query($conn, $sqlLopp);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ruumide broneerimine</title>
    <?php include("header.html"); ?>
</head>
<body>
<header>
    <?php include("nav2.html"); ?>
</header>
<div class="container" id="tableContainer">
    <?php include("ruumiDetailvaade.php"); ?>
    <?php include("broneeringud.php"); ?>
</div>
</body>
</html>