<?php
include 'config.php';

if ($pass1 == $pass2) {
    //vaatan kas selline kasutajanimi juba eksisteerib
    $sqlcheck = mysqli_query($conn, "SELECT * FROM kasutaja WHERE kasutajanimi = '$newusername'");
    if (mysqli_num_rows($sqlcheck) > 0) {
        header('Location: changeUsernameFailure.php');
        exit();
    }
    //kontrollin salasõna, et oleks aktiivse kasutaja oma
    $aktiivneKasutajaParool = "SELECT parool FROM kasutaja WHERE kasutajanimi=(SELECT kasutajanimi FROM aktiivnekasutaja ORDER BY ID DESC limit 1)";
    $jooks = mysqli_query($conn, $aktiivneKasutajaParool);
    $seePassiRida = mysqli_fetch_assoc($jooks);
    $actuallySeeParool = $seePassiRida['parool'];

    if ($actuallySeeParool == $pass1) {

        $aktiivneKasutajaID = "SELECT kasutaja_id FROM kasutaja WHERE kasutajanimi=(SELECT kasutajanimi FROM aktiivnekasutaja ORDER BY ID DESC limit 1)";
        $seequel = mysqli_query($conn, $aktiivneKasutajaID);
        $seeRida = mysqli_fetch_assoc($seequel);
        $actuallySeeID = $seeRida['kasutaja_id'];

        $sql = "UPDATE kasutaja SET kasutajanimi = '$newusername' WHERE kasutaja_id = '$actuallySeeID'";

        if ($conn->query($sql) == TRUE) {
            header('Location: changeUsernameSuccess.php');
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
    } else {
        header('Location: changeUsernameFailure.php');
    }
} else {
    header('Location: changeUsernameFailure.php');
    exit();
}