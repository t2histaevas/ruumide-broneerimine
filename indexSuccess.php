<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ruumide broneerimine</title>
    <?php include("header.html"); ?>

</head>
<body>
<header>
    <?php include("nav.html"); ?>
</header>
    <div class="container" id="mainContainer">
        <div class="container" id="container1">
            <div id="edukasRega" class="alert alert-success">
                <p>Olete edukalt registreeritud!</p>
                <p style="margin-top: 20px;"></p>
                <p>Logige sisse</p>
            </div>
        </div>
    </div>
</body>
</html>