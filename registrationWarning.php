<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sisse logimine</title>
    <?php include("header.html"); ?>
</head>
<body>
<header>
    <?php include("nav.html"); ?>
</header>
    <?php include("registrationForm.html"); ?>
    <div class="container" id="container1">
        <div class="alert alert-warning">
            <strong>Viga!</strong> Selline kasutajanimi on juba kasutusel.
        </div>
    </div>
</body>
</html>