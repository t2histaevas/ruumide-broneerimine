<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sisse logimine</title>
    <?php include("header.html"); ?>
</head>
<body>
<header>
    <?php include("nav.html"); ?>
</header>
    <?php include("loginForm.html"); ?>
    <div class="container" id="container1">
        <div class="alert alert-warning">
            <strong>Viga!</strong> Vale kasutajanimi või parool.
        </div>
    </div>
</body>
</html>