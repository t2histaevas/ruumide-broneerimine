<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ruumide broneerimine</title>
    <?php include("header.html"); ?>
</head>
<body>
<header>
    <?php include("nav2.html"); ?>
</header>
<div class="container" id="tableContainer">
    <?php

    include 'config.php';

    $sql = "SELECT * FROM aktiivnekasutaja ORDER BY ID DESC limit 1";

    $records = mysqli_query($conn, $sql);
    $kasutajanimi = mysqli_fetch_assoc($records);

    echo "<h1 style='text-align: left'>".'Tere '.$kasutajanimi['kasutajanimi'].'!'."</h1>"
    ?>
    <h1 id="ruumidPealkiri">Ruumid</h1>
    <?php include 'getData.php';?>
</div>
</body>
</html>