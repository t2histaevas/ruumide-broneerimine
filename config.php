<?php
//phpmyAdminis oleva andmebaasiga ühendamine
$servername = "localhost";
$dbusername = "test";
$dbpassword = "t3st3r123";
$dbname = "test";

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);

//muutujad...toimub ka nende filtreerimine ja sanitizemine xss vastu
$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$pass1 = filter_input(INPUT_POST, 'pass1', FILTER_SANITIZE_STRING);
$pass2 = filter_input(INPUT_POST, 'pass2', FILTER_SANITIZE_STRING);
$algus = filter_input(INPUT_POST, 'algus', FILTER_SANITIZE_STRING);
$lopp = filter_input(INPUT_POST, 'lopp', FILTER_SANITIZE_STRING);
$pass = filter_input(INPUT_POST, 'pass', FILTER_SANITIZE_STRING);
$newusername = filter_input(INPUT_POST, 'newusername', FILTER_SANITIZE_STRING);