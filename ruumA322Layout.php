<?php

$ruumi_id = 2;
$sql = "SELECT * FROM ruum WHERE ruumi_id = '$ruumi_id'";

$records = mysqli_query($conn, $sql);

$bronsql = "SELECT * FROM broneering WHERE ruumi_id = '$ruumi_id'";

$bronrecords = mysqli_query($conn, $bronsql);

$sqlAlgus = "SELECT bronni_algus FROM broneering WHERE ruumi_id = '$ruumi_id'";

$sqlLopp = "SELECT bronni_lopp FROM broneering WHERE ruumi_id = '$ruumi_id'";

$broneeringuSooritanudKasutaja = "SELECT kasutaja_id FROM broneering WHERE ruumi_id = '$ruumi_id'";

$broneeringAlgusRecords = mysqli_query($conn, $sqlAlgus);

$broneeringLoppRecords = mysqli_query($conn, $sqlLopp);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ruumide broneerimine</title>
    <?php include("header.html"); ?>
</head>
<body>
<header>
    <?php include("nav2.html"); ?>
</header>
<div class="container" id="tableContainer">
    <?php include("ruumiDetailvaade.php"); ?>
    <?php include("broneeringud.php"); ?>
</div>
</body>
</html>