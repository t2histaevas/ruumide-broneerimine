<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ruumide broneerimine</title>
    <?php include 'header.html';?>
</head>
<body>
<header>
    <?php include 'nav2.html';?>
</header>
<div class="container" id="tableContainer">
    <h1 id="ruumidPealkiri">Ruumid</h1>
    <?php include 'getData.php';?>
    <p style="margin-top: 20px"></p>
    <div class="alert alert-warning">
        Broneerimine ebaõnnestus: valitud ajal on ruum juba broneeritud
    </div>
</div>
</body>
</html>