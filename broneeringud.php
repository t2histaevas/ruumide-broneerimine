<h3>Lisa oma broneering: </h3>
<form class="form-bron" method="post" action="insertBroneering.php">
    <label for="inputEmail" class="sr-only">Algus</label>
    <input type="text" id="inputEmail" class="form-control" name="algus" placeholder="Algus (YYYY-MM-DD HH:MI:SS)" required autofocus>
    <p style="margin-top: 10px;"></p>
    <label for="inputPassword" class="sr-only">Lõpp</label>
    <input type="text" id="inputPassword" class="form-control" name="lopp" placeholder="Lõpp (YYYY-MM-DD HH:MI:SS)" required>
    <p style="margin-top: 10px;"></p>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Broneeri</button>
</form>

<h3>Olemasolevad broneeringud: </h3>
<table>
    <tr>
        <th style="text-align: center">Broneerija</th>
        <th style="text-align: center">Broneeringu algus</th>
        <th style="text-align: center">Broneeringu lõpp</th>
    </tr>
        <?php
        for ($x = 0; $x <= mysqli_num_rows($bronrecords); $x++) {
            echo "<tr>";
            $bronTegija = mysqli_fetch_assoc($bronrecords);
            $bronTegijaID = $bronTegija['kasutaja_id'];
            $bronTegijaNimisql = "SELECT kasutajanimi FROM kasutaja WHERE kasutaja_id = '$bronTegijaID'";
            $bronTegijaRecords = mysqli_query($conn, $bronTegijaNimisql);
            $bronTegijaNimi = mysqli_fetch_assoc($bronTegijaRecords);
            echo "<td>".$bronTegijaNimi['kasutajanimi']."</td>";
            $bronAlgus = mysqli_fetch_assoc($broneeringAlgusRecords);
            echo "<td>".$bronAlgus['bronni_algus']."</td>";
            $bronLopp = mysqli_fetch_assoc($broneeringLoppRecords);
            echo "<td>".$bronLopp['bronni_lopp']."</td>";
            echo "</tr>";
        }
        ?>
</table>