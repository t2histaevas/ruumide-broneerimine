<?php
include "config.php";

//kontrollin, kas kasutaja on sisse logitud. Vastasel juhul tagasi kodulehele
$sqlcheck = mysqli_query($conn, "SELECT * FROM aktiivnekasutaja");
if (mysqli_num_rows($sqlcheck) != 0) {
    include "mainLayout.php";
} else {
    header('Location: index.php');
}
$conn -> close();